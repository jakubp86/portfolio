const clock = ()=>{
    const date = new Date();
    const day = date.getDate();
    const month = date.getMonth()+1;
    const year = date.getFullYear();
    const hour = date.getHours();
    const minute = date.getMinutes();
    const seconds = date.getSeconds();
    return `${landingZero(day)}.${landingZero(month)}.${year}`
    // return `${landingZero(hour)}:${landingZero(minute)}:${landingZero(seconds)}`;
}

const landingZero = (num)=>{
    return num < 10 ? `0${num}` : num
}
const display = ()=>{
    const container =document.querySelector('.small-square');
    container.innerHTML = clock();
}
display()
// setInterval(()=>display(),1000)